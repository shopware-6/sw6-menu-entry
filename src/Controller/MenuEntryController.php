<?php declare(strict_types=1);

namespace JMSE\MenuEntry\Controller;

use Shopware\Storefront\Controller\StorefrontController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route(defaults={"_routeScope"={"store-api"}})
 */
class MenuEntryController extends StorefrontController
{
    /**
     * @Route("/store-api/hello-world", name="store-api.helloworld", methods={"GET"})
     */
    public function helloworld()
    {
        echo 'This is a hello world page!';
        exit;
    }
}